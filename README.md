## Branches

It's imperative that you use git-flow and undertand the concept behind.
So please take 5 min to read [Vincent Driessen’s branching model](http://nvie.com/posts/a-successful-git-branching-model/), even if you already know,  it's always good to remember.

> This model will be shared across all projects. It's easy to comprehend and give us knowledge and control of the branching and releasing processes.

Here are some ground rules:
 - The only branches that will exist forever are **master** and **develop**
 - All the support branches are temporary and must be created with the prefix **feature/**, **release/** or **hotfix/** and immediately deleted after it's merged back. If you want to keep track for some reason, use a **tag** instead of the branch itself
 - You should never commit on **master**, it will only receive merges from **hotfix/** or **release/** and the merge must always tagged with the it's version
 - Supporting branches
     + **feature**:
         * May branch off from: **develop**
         * Must merge back into: **develop**
     + **release**:
         * May branch off from: **develop**
         * Must merge back into: **develop** AND **master**
     + **hotfix**:
         * May branch off from: **master**
         * Must merge back into: **develop**

You can use any tool you want as long it's supports git-flow.
There is also a realy nice [git extension](https://github.com/nvie/gitflow) for those of us who like using the terminal.

___
## License

Copyright © 2018 SkyTest. All rights reserved.