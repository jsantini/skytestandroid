package com.sky.android.skytest.ui.movieList

import android.arch.lifecycle.MutableLiveData
import com.sky.android.skytest.base.BaseViewModel
import com.sky.android.skytest.model.Movie


class MovieViewModel:BaseViewModel() {
    private val movieTitle = MutableLiveData<String>()
    private val movieDuration = MutableLiveData<String>()
    private val movieCoverUrl = MutableLiveData<String>()

    fun bind(movie: Movie) {
        movieTitle.value = movie.title
        movieDuration.value = movie.duration
        movieCoverUrl.value = movie.coverUrl
    }

    fun getMovieTitle(): MutableLiveData<String> {
        return movieTitle
    }

    fun getMovieDuration(): MutableLiveData<String> {
        return movieDuration
    }

    fun getMovieCoverUrl(): MutableLiveData<String> {
        return movieCoverUrl
    }

    fun onItemClickListener(movie: Movie) {
    }
}