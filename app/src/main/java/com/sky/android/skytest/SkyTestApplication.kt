package com.sky.android.skytest

import android.app.Application

class SkyTestApplication: Application() {

    //lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        instance = this
        setup()

    }

    fun setup() {

//        component = DaggerApplicationComponent.builder()
//                .applicationModule(ApplicationModule(this, Environment.BASE_URL)).build()
//        component.inject(this)
//
//        skyTestComponent = DaggerSkyTestComponent.builder()
//                .appComponent(component)
//                .build()
    }

//    fun getApplicationComponent(): ApplicationComponent {
//        return component
//    }

    companion object {
        lateinit var instance: SkyTestApplication private set
    }
}