package com.sky.android.skytest.api

import com.sky.android.skytest.model.Movie
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiSky {
    @GET("Movies")
    fun getMovies(): Observable<List<Movie>>
}