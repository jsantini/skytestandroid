package com.sky.android.skytest.base

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.sky.android.skytest.R

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        overridePendingTransition(R.anim.slide_in_rigth_to_left, R.anim.slide_out_rigth_to_left)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left_to_rigth, R.anim.slide_out_left_to_rigth)
    }

    fun AppCompatActivity.addFragment(containerId: Int, fragment: Fragment, tag: String = "") {
        supportFragmentManager.beginTransaction().add(containerId, fragment, tag).commit()
    }
}
