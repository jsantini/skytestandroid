package com.sky.android.skytest.di.component

import com.sky.android.skytest.di.module.NetworkModule
import com.sky.android.skytest.ui.movieList.MovieListViewModel
import com.sky.android.skytest.ui.movieList.MovieViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    fun inject(movieListViewModel: MovieListViewModel)

    fun inject(movieViewModel: MovieViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}