package com.sky.android.skytest.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.sky.android.skytest.model.Movie
import com.sky.android.skytest.ui.movieDetail.MovieDetailViewModel

class MovieDetailViewModelFactory(private val movie: Movie): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieDetailViewModel::class.java)) {
            return MovieDetailViewModel(movie) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}