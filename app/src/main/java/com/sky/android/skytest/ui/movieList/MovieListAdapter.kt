package com.sky.android.skytest.ui.movieList

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.sky.android.skytest.R
import com.sky.android.skytest.databinding.ItemMovieBinding
import com.sky.android.skytest.model.Movie
import com.sky.android.skytest.ui.movieDetail.MovieDetailActivity

class MovieListAdapter: RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {
    private lateinit var movieList:List<Movie>
    private lateinit var context:Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListAdapter.ViewHolder {
        val binding: ItemMovieBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_movie, parent, false)
        context = parent.context
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieListAdapter.ViewHolder, position: Int) {
        var movie = movieList[position]
        holder.bind(movie)
        holder.itemView.setOnClickListener {
            context.startActivity(MovieDetailActivity.getStartIntent(context, movie))
        }
    }

    override fun getItemCount(): Int {
        return if(::movieList.isInitialized) movieList.size else 0
    }

    fun updateMovieList(movieList:List<Movie>){
        this.movieList = movieList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemMovieBinding): RecyclerView.ViewHolder(binding.root){
        val viewModel = MovieViewModel()
        fun bind(movie:Movie){
            viewModel.bind(movie)
            binding.viewModel = viewModel
        }
    }

}