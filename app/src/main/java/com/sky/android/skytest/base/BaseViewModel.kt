package com.sky.android.skytest.base

import android.arch.lifecycle.ViewModel
import com.sky.android.skytest.di.component.DaggerViewModelInjector
import com.sky.android.skytest.di.component.ViewModelInjector
import com.sky.android.skytest.di.module.NetworkModule
import com.sky.android.skytest.ui.movieList.MovieListViewModel
import com.sky.android.skytest.ui.movieList.MovieViewModel

abstract class BaseViewModel: ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is MovieListViewModel -> injector.inject(this)
            is MovieViewModel -> injector.inject(this)
        }
    }
}