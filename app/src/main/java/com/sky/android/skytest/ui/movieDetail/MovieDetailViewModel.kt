package com.sky.android.skytest.ui.movieDetail

import android.arch.lifecycle.MutableLiveData
import com.sky.android.skytest.base.BaseViewModel
import com.sky.android.skytest.model.Movie

class MovieDetailViewModel(movie:Movie): BaseViewModel() {
    private val movieTitle = MutableLiveData<String>()
    private val movieDuration = MutableLiveData<String>()
    private val movieCoverUrl = MutableLiveData<String>()
    private val movieReleaseYear = MutableLiveData<String>()
    private val movieOverview = MutableLiveData<String>()

    init {
        movieTitle.value = movie.title
        movieDuration.value = movie.duration
        movieCoverUrl.value = movie.coverUrl
        movieOverview.value = movie.overview
        movieReleaseYear.value = movie.releaseYear

    }

    fun getMovieTitle(): MutableLiveData<String> {
        return movieTitle
    }

    fun getMovieDuration(): MutableLiveData<String> {
        return movieDuration
    }

    fun getMovieCoverUrl(): MutableLiveData<String> {
        return movieCoverUrl
    }

    fun getMovieReleaseYear() : MutableLiveData<String> {
        return movieReleaseYear
    }

    fun getMovieOverview() : MutableLiveData<String> {
        return movieOverview
    }
}