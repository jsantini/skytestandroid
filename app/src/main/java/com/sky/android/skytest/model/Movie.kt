package com.sky.android.skytest.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Movie(
        @SerializedName("title") @Expose val title: String,
        @SerializedName("overview") @Expose val overview: String,
        @SerializedName("duration") @Expose val duration: String,
        @SerializedName("release_year") @Expose val releaseYear: String,
        @SerializedName("cover_url") @Expose val coverUrl: String,
        @SerializedName("backdrops_url") @Expose val backDropsUrl: List<String>

) {
}

