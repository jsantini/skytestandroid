package com.sky.android.skytest.ui.movieDetail

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.sky.android.skytest.R
import com.sky.android.skytest.base.BaseActivity
import com.sky.android.skytest.di.MovieDetailViewModelFactory
import com.sky.android.skytest.model.Movie
import com.sky.android.skytest.ui.movieList.MovieViewModel
import com.sky.android.skytest.util.Constants
import kotlinx.android.synthetic.main.activity_movie_list.*

class MovieDetailActivity : BaseActivity() {

    companion object {

        fun getStartIntent(context: Context, movie: Movie): Intent {
            val intent = Intent(context, MovieDetailActivity::class.java)
            intent.putExtra(Constants.EXTRA_MOVIE_JSON, Gson().toJson(movie))
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        addFragment(R.id.content_frame, createFragment())
    }

    fun createViewModel(): MovieDetailViewModel {
        var movie = Gson().fromJson(intent.getStringExtra(Constants.EXTRA_MOVIE_JSON), Movie::class.java)
        return ViewModelProviders.of(this, MovieDetailViewModelFactory(movie)).get(MovieDetailViewModel::class.java)
    }

    fun createFragment(): MovieDetailFragment {
        return MovieDetailFragment.newInstance(createViewModel())
    }

    fun setupToolbar() {
        setSupportActionBar(toolbar)
    }
}
