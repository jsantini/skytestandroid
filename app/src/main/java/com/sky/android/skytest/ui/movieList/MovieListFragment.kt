package com.sky.android.skytest.ui.movieList

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sky.android.skytest.R
import com.sky.android.skytest.databinding.MovieListFragmentBinding


class MovieListFragment : Fragment() {

    private lateinit var viewModel: MovieListViewModel
    private lateinit var binding: MovieListFragmentBinding

    companion object {
        fun newInstance(viewModel: MovieListViewModel): MovieListFragment {
            val fragment = MovieListFragment()
            fragment.viewModel = viewModel
            return fragment
        }
    }



    override fun onCreateView(@NonNull inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.movie_list_fragment, container, false)
        binding.viewModel = viewModel
        binding.movieList.layoutManager = GridLayoutManager(activity, 2)
        binding.movieList.setHasFixedSize(true)
        viewModel.errorMessage.observe(this, Observer {
            errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError()
        })
        return binding.root
    }

    private fun showError(@StringRes errorMessage:Int){
        binding.errorMessage.setText(errorMessage)
        binding.contentError.setVisibility(View.VISIBLE)
    }

    private fun hideError(){
        binding.contentError.setVisibility(View.GONE)
    }
}