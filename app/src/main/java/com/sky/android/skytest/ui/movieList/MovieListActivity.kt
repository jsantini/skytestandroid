package com.sky.android.skytest.ui.movieList

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.sky.android.skytest.R
import com.sky.android.skytest.base.BaseActivity
import com.sky.android.skytest.di.MovieListViewModelFactory
import kotlinx.android.synthetic.main.activity_movie_list.*

class MovieListActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list)

        setupToolbar()

        addFragment(R.id.content_frame, createFragment())

    }

    fun setupToolbar() {
        toolbar.setTitle(R.string.title_view_movies_list)
        setSupportActionBar(toolbar)
    }

    fun createViewModel(): MovieListViewModel {
        return ViewModelProviders.of(this, MovieListViewModelFactory(this)).get(MovieListViewModel::class.java)
    }

    fun createFragment(): MovieListFragment {
        return MovieListFragment.newInstance(createViewModel())
    }

}
