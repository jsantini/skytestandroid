package com.sky.android.skytest.ui.movieList

import android.arch.lifecycle.MutableLiveData
import android.view.View
import com.sky.android.skytest.R
import com.sky.android.skytest.api.ApiSky
import com.sky.android.skytest.base.BaseViewModel
import com.sky.android.skytest.model.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieListViewModel() : BaseViewModel() {

    @Inject
    lateinit var apiSky: ApiSky

    val movieListAdapter: MovieListAdapter = MovieListAdapter()

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val errorMessage:MutableLiveData<Int> = MutableLiveData()

    private lateinit var subscription: Disposable

    init{
        loadMovies()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun loadMovies(){
        subscription = apiSky.getMovies().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onLoadMovieListStart() }
                .doOnTerminate { onLoadMovieListFinish() }
                .subscribe({ list: List<Movie> ->
                    onLoadMovieListSuccess(list)
                }, { error ->
                    onLoadMovieListError()
                })
    }

    private fun onLoadMovieListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onLoadMovieListFinish(){
        loadingVisibility.value = View.GONE
    }

    private fun onLoadMovieListSuccess(movieList: List<Movie>){
        movieListAdapter.updateMovieList(movieList)
    }

    private fun onLoadMovieListError(){
        errorMessage.value = R.string.get_movies_error
    }

}
