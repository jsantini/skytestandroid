package com.sky.android.skytest.ui.movieDetail

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sky.android.skytest.R
import com.sky.android.skytest.databinding.MovieDetailFragmentBinding

class MovieDetailFragment : Fragment() {

    private lateinit var viewModel: MovieDetailViewModel

    companion object {
        fun newInstance(viewModel: MovieDetailViewModel): MovieDetailFragment {
            val fragment = MovieDetailFragment()
            fragment.viewModel = viewModel
            return fragment
        }
    }

    override fun onCreateView(@NonNull inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : MovieDetailFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.movie_detail_fragment, container, false)
        binding.viewModel = viewModel
        setupToolbar()
        return binding.root
    }

    fun setupToolbar() {
        (activity as MovieDetailActivity).setupToolbar()
    }
}